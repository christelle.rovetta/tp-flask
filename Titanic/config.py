import os

# Title
title = 'Titanic blup blup'

# Navbar
d_navbar = {'Accueil Blup blup': ['accueil', 1], 'Questionnaire': ['question', 1], 'Blup': ['blup', 0] }

# Data path
data_path = os.path.join("application", "static", "data" "test.csv")

# Questions path
q_list_path = os.path.join("application","static","json","question_list.json")

# Model
model_path = os.path.join("application", "static","model","my_model.pkl")

# Image path
img_path = os.path.join("application","static","img","titanic.jpg")

# Global information
INFO = {

    # Affichage dans la barre de navigation
    'nav': d_navbar,
    'user': 'Blup blup',
    'projet': 'Titanic',

    # Data
    'data_path': data_path,

    # Model
    'model_path': model_path,

    # Questions
    'q_list_path': q_list_path,

    # Image
    'img_path' : img_path
    }
