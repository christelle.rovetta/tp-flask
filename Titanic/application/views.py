from flask import Flask, render_template, url_for, request

import json
import pandas as pd
import time

# Import de l'objet Prediction
from .core import Prediction

# App flask
app = Flask(__name__)  

# Config options - Make sure you created a 'config.py' file.
app.config.from_object('config') # To get one variable, tape app.config['MY_VARIABLE']

global info
global predictor 

# Configuration
info = app.config['INFO']

# Modele pour la prédiction
predictor = Prediction(info['model_path'])


@app.route('/question/', methods = ['GET', 'POST', 'DELETE'])
def question():

    """ Page principale: le questionnaire """

    data_question = dict()

    # Import de la liste des questions sous forme de dictionnaire
    with open(info['q_list_path']) as json_data:
        data_question = json.load(json_data)

    return render_template('questions.html', info = info, data = data_question)


@app.route('/result/', methods = ['GET', 'POST', 'DELETE'])
def result():

    """ Page du résultat  """

    # Résultat par défaut
    data_result = "Je ne sais pas ..."

    # Récupération des réponses dans le GET
    if  request.method == 'GET':

        # Dictionnaire des réponses (plus facile pour construire une dataframe)
        d_reponse = dict()

        # Ouverture du fichier des questions
        with open(info['q_list_path']) as json_data:
            data_question = json.load(json_data)

            # Récupération des réponses question par question
            for id_q in data_question:

                d_reponse[id_q] = list()
                
                # On recupere la valeur de la question
                if request.args.get(id_q):
                    value = request.args.get(id_q)
                    d_reponse[id_q].append(value)

            # Typage (Pclass, SibSp, Parch): transformation str->int
            d_reponse['Pclass'] = [int(d_reponse['Pclass'][0])]
            d_reponse['SibSp'] = [int(d_reponse['SibSp'][0])]
            d_reponse['Parch'] = [int(d_reponse['Parch'][0])]

            # Questions manquantes
            d_reponse['Fare'] = [None]
            d_reponse['Age'] = [None]   # Attention exercice 2
            d_reponse['Cabin'] = [None]

            # Dataframe des résultats
            df_reponse = pd.DataFrame(d_reponse)

            # !!! ICI pour la prediction !!!!
            
            # Pour l'affichage du resultat (a modifier si prediction)
            data_result += "<br> mais voici tes réponses : <br> " + df_reponse.to_html()

    return render_template('result.html', info = info, data = data_result)

@app.route('/', methods = ['GET', 'POST', 'DELETE'])
@app.route('/index/', methods = ['GET', 'POST', 'DELETE'])
@app.route('/accueil/', methods = ['GET', 'POST', 'DELETE'])
def index():

    """ Page d'accceuil  """

    data_index = "Je ne sais pas quoi dire de plus ... blup blup"

    return render_template('index.html', info = info, data = data_index)

@app.route('/blup/', methods = ['GET', 'POST', 'DELETE'])
def blup():

    """ Test javascrip  """

    data_index = "Je ne sais pas quoi dire de plus ... blup blup"

    return render_template('blup.html', info = info, data = data_index)


if __name__ == "__main__":
    app.run()

