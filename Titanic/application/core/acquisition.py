import os
import pandas as pd

def load_data_from_csv(data_path):
    
    """ Recupere les données csv sous forme de dataframe """
    
    df = pd.read_csv(data_path, low_memory = False)
    
    return df