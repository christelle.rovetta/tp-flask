import pandas as pd

def feature_cat(df, d_cat:dict):
    
    """ 
        Preprocess des variables categorielles
        ex: d_cat = {'node_os': ['LINUX', 'WIN'], 'hote_os': ['VMWARE']}
    """
    
    def is_value(name, value):
        
        if name == value:
            return 1
        
        return 0
     
    # Dataset restreint
    label_cat = list(d_cat.keys()) # liste des variables cat
    df_cat = df[label_cat]
    
    # Remplacement des valeurs manquantes
    values = dict()
    for k in d_cat:
        values[k] = 'no'
    df_cat = df_cat.fillna(value=values)
    
    # Encodage one hot maison
    l_features = list()
    for k in d_cat:
        for v in d_cat[k]:
            df_cat.loc[:, v] = df_cat[k].apply(is_value, args=(v,))
            l_features.append(v)

    # On garde les variables qui nous interesse
    df_cat = df_cat[l_features]
    
    return df_cat

def preprocess_titanic(df):
    
    """ Preprocess data : data cleaning + feature engeneering """

    # Age
    a_miss = 12 #22
    a_yman = 18 #32
    a_pro = 49
    a_mean = 30

    # Fare
    fare_1 = 85
    fare_2 = 20
    fare_3 = 13
    
    features = ['Pclass', 'Name', 'Sex', 'Age', 'SibSp','Parch', 'Fare', 'Cabin', 'Embarked']
    X =  df[features]
    
    # Extract civility from name: Miss, Mrs, Mr, Pro, Nob
    def civility(val):
        t_val = val.split(',')
        civ = t_val[1].split('.')[0]
        if civ == ' Mlle' or civ == ' Miss':
            return 'Miss'
        if civ == ' Mrs' or civ == ' Mme':
            return 'Mrs'
        if civ == ' Mr' or civ == ' Ms':
            return 'Mr'
        if civ == ' Major' or civ == ' Col' or civ == ' Capt' or civ == ' Dr':
            return 'Pro'
        return 'Nob'
    
    X['Civility'] = X['Name'].astype(str)
    X['Civility'] = X['Civility'].apply(civility)
    
    # Replace missing values
    values = {'Cabin': '', 'Embarked': 'M', 'Age':-1, 'Fare':-1}
    X = X.fillna(value=values)
    
    # Label encoding : Sex
    X= X.replace({'Sex': {'male': 0, 'female': 1}})

    # Label encoding : Cabin (nan -> 0, B,C -> 3, other 1)
    def cabin_score(val):
        if val == '':
            return 0
        if val[0] == 'C' or val[0] == 'B':
            return 2
        return 1
    X['Cabin'] = X['Cabin'].astype(str)
    X['Cabin'] = X['Cabin'].apply(cabin_score)
    
    # One Hot Encoding : Embarked and Civility
    d_onehot = dict() 
    d_onehot['Embarked'] = ['C', 'S', 'Q']
    d_onehot['Civility'] = ['Miss', 'Mrs', 'Mr', 'Pro', 'Nob']
    df_hot = feature_cat(X, d_onehot)
    
    num_X = X.drop(['Embarked'], axis=1)
    X = pd.concat([num_X, df_hot], axis=1)
    
    # Heuristic : Age (guess missing age)
    def age(col):
        if col['Age'] == -1:
            if col['Civility'] == 'Miss': # Miss
                return a_miss
            if col['Civility'] == 'Mr' and col['SibSp'] == 0 and col['Parch'] == 0: # Young man
                return a_yman
            if col['Civility'] == 'Pro': # Army or Dr
                return a_pro
            return a_mean
        return col['Age']
    X['Age'] = X.apply(age, axis=1)
    
    # Heuristic : Fare (guess missing fare)
    def fare(col):
        if col['Fare'] == -1:
            if col['Pclass'] == 3:
                return fare_3
            if col['Pclass'] == 2:
                return fare_2
            if col['Pclass'] == 1:
                return fare_1
        return col['Fare']
    X['Fare'] = X.apply(fare, axis=1)
                
    # 4. Drop columns without interrest
    X = X.drop(['Name', 'Civility'], axis=1)
    
    return X