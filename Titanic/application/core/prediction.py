import numpy as np
import pandas as pd
from xgboost import XGBClassifier

from sklearn.metrics import confusion_matrix
import joblib

from .acquisition import load_data_from_csv
from .preprocess import preprocess_titanic


class Prediction:

    """ Classe des predictions """

    def __init__(self, model_path: str):

        """ Constructeur """

        # Load model
        self.model = joblib.load(model_path)

    def prediction(self, df_reponse):
        
        """ Prédiction """
        
        # Preprocess
        df_clean = preprocess_titanic(df_reponse)
        
        # Prediction
        pred = self.model.predict(df_clean.to_numpy())
        
        return pred


    

    

