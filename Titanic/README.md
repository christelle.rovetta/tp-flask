# Titanic

Le projet Titanic a pour but l'apprentissage de la construction d'un IHM  utilisant le package flask dans le cadre d'un projet de machine learning. 


## 1. Utilisation

Les chemins vers les données ainsi que le modéle utilisé sont renseignés dans le fichier conf.py :

```python 
# Data path
data_path = r"application\static\data\mini_dataset.csv"

# Model
model_path = r"application\static\model\1_XGBClassifieS.pkl"
```

L'application requiert les packages python suivant : flask, pandas,  scikit-learn et XGBoost. Flask, pandas et  scikit-learn sont disponibles dans l'environement anaconda. 

XGboost doit etre installé (via pip par exemple).

Pour lancer l'application : `>> python app.py`

## 2. Architecture du projet Titanic

application/                    package python
├── core                        package python
│   ├── acquisition.py          module pour l'aquisition des données
│   ├── __init__.py
│   ├── prediction.py           module pour la prédiction
│   ├── preprocess.py           module pour le traitement des données
├── __init__.py
├── static                      dossier contenant les fichiers statiques
│   ├── css                     dossier des fichiers css
│   │   └── datalab.css
│   ├── data                    dossier des fichiers de données
│   │   └── test.csv
│   ├── img                     dossier des images
│   │   └── titanic.jpg
│   ├── json                    dosssier des données encodées en json
│   │   └── question_list.json
│   └── model                   dossier des modeles de machine learning
│       └── my_model.pkl
├── templates                   dossiers des pages html
│   ├── base.html               template de page html
│   ├── index.html              page d'aceuil
│   ├── questions.html          
│   ├── result.html
│   └── utils
│       └── navbar.html         bare de navigation
└── views.py                    


